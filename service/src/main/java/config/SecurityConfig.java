package config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import javax.sql.DataSource;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    DataSource dataSource;

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth.jdbcAuthentication().dataSource(dataSource).passwordEncoder(new BCryptPasswordEncoder()).usersByUsernameQuery("select login,password ,enabled from user where login=?")
                .authoritiesByUsernameQuery(
                        "select login,role from user where login=?");
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http.authorizeRequests()
                .antMatchers(HttpMethod.POST,"/user/**").access("hasRole('ROLE_ADMIN')")
                .antMatchers(HttpMethod.GET,"/user/**").access("hasRole('ROLE_ADMIN')")
                .antMatchers(HttpMethod.POST,"/product/**").access("hasRole('ROLE_ADMIN')")
                .antMatchers(HttpMethod.POST,"/order/**").access("hasRole('ROLE_ADMIN')")
                .antMatchers(HttpMethod.GET,"/order/**").access("isAuthenticated()")
                .antMatchers(HttpMethod.POST,"/category/**").access("hasRole('ROLE_ADMIN')")
                .and().formLogin().loginPage("/login").defaultSuccessUrl("//localhost:63343/client/index.html?_ijt=qju5v8e2doa2qg8u71somdm9br#/", true)
                .failureUrl("//localhost:63343/client/index.html?_ijt=qju5v8e2doa2qg8u71somdm9br#/login?error").permitAll()
                .and().headers().disable();

        //Cross Site Request Forgery
        http.csrf().disable();



    }
}
