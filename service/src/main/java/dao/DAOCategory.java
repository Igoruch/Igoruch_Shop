package dao;

import model.Category;
import java.util.List;


public interface DAOCategory extends GenericDao<Category> {

    void deleteCategory(Category category);

    List<Category> getPageOfCategory(int page,int count);
}
