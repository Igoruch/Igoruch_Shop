package dao;

import model.Product;
import java.util.List;

public interface DAOProduct extends GenericDao<Product>{

    List<Product> getPageOfProductsByCategoryId(int categoryId, int page, int count);

    List<Product> getProductsByCategoryId(int categoryId);

    List<Product> getLastAddedProducts(int count);

    List<Product> getProductByOrderId(int id);
}
