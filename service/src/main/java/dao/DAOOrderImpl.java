package dao;

import model.Order;
import org.apache.log4j.Logger;
import javax.persistence.Query;
import java.util.List;


public class DAOOrderImpl extends GenericDaoImpl<Order> implements DAOOrder {

    private final String getAllOrdersInProcessQuery = "SELECT o FROM Order o WHERE o.status = 'In_process'";

    private static final Logger log = Logger.getLogger(DAOOrderImpl.class);

    @Override
    public List<Order> getAllOrdersInProcess() {
        log.info("Get all order in process!");
        Query query = em.createQuery(getAllOrdersInProcessQuery);
        return (List<Order>) query.getResultList();
    }
}
