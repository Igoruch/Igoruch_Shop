package dao;

import org.springframework.beans.factory.annotation.Autowired;

public class DAOFactoryImpl implements DAOFactory {

    @Autowired
    private DAOUser userDAO;

    @Autowired
    private DAOProduct productDAO;

    @Autowired
    private DAOFile fileDAO;

    @Autowired
    private DAOCategory categoryDAO;

    @Autowired
    private DAOOrder orderDAO;

    @Override
    public DAOUser getUserDAO() {
        return userDAO;
    }

    @Override
    public DAOProduct getProductDAO() {
        return productDAO;
    }

    @Override
    public DAOFile getFileDAO() {
        return fileDAO;
    }

    @Override
    public DAOCategory getCategoryDAO() {
        return categoryDAO;
    }

    @Override
    public DAOOrder getOrderDAO() {
        return orderDAO;
    }
}
