package dao;

import org.apache.log4j.Logger;
import org.springframework.web.multipart.MultipartFile;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;

/**
 * Created by Igoruch on 24.02.2017.
 */
public class DAOFileImpl implements DAOFile{

    private static final Logger log = Logger.getLogger(DAOFileImpl.class);

    @Override
    public void deleteFile(String path) {
        try{
            File file = new File(path);
            if(file.delete()){
                log.info(file.getName() + " is deleted!");
            }
        }catch(Exception e){
            log.error(e.getMessage());
            e.printStackTrace();
        }
    }

    @Override
    public void uploadImage(MultipartFile file, String path) {
        String fileName = null;
        try {
        fileName = file.getOriginalFilename();
        byte[] bytes = file.getBytes();
        BufferedOutputStream buffStream =
                new BufferedOutputStream(new FileOutputStream(new File(path + fileName)));
        buffStream.write(bytes);
        buffStream.close();
        log.info(file.getName() + " is upload!");
        }
        catch (Exception e){
            log.error("Failed to upload " + fileName + ": " + e.getMessage());
        }
    }
}
