package dao;

import model.User;
import org.apache.log4j.Logger;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

public class DAOUserImpl extends GenericDaoImpl<User> implements DAOUser {

    private final String getUserByNameQuery = "select u from User u where u.login = :Name";
    private final String getUserByOrderIdQuery = "SELECT u FROM User u JOIN u.ordersSet o WHERE o.id = :idOrder";

    private static final Logger log = Logger.getLogger(DAOProductImpl.class);

    @Override
    public User getUserByName(String name){
        log.info("Get user by name: "+name);
        TypedQuery<User> namedQuery = em.createQuery(getUserByNameQuery, User.class).setParameter("Name",name);
        if (namedQuery.getResultList().isEmpty()){
            return null;
        }else return namedQuery.getSingleResult();
    }

    @Override
    public User getUserByOrderId(int id) {
        log.info("Get user by order id: "+id);
        Query query = em.createQuery(getUserByOrderIdQuery);
        query.setParameter("idOrder", id);
        return (User)query.getSingleResult();
    }
}
