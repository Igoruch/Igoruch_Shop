package dao;

public interface DAOFactory {

    DAOUser getUserDAO();

    DAOProduct getProductDAO();

    DAOFile getFileDAO();

    DAOCategory getCategoryDAO();

    DAOOrder getOrderDAO();
}
