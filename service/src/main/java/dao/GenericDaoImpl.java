package dao;

import org.apache.log4j.Logger;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

public class GenericDaoImpl<T> implements GenericDao<T> {

    public EntityManager em;

    private Class<T> type;

    private static final Logger log = Logger.getLogger(DAOProductImpl.class);

    public GenericDaoImpl() {
        em = Persistence.createEntityManagerFactory("COLIBRI").createEntityManager();
        Type t = getClass().getGenericSuperclass();
        ParameterizedType pt = (ParameterizedType) t;
        type = (Class) pt.getActualTypeArguments()[0];
    }

    @Override
    public T add(T t) {
        log.info("Add entity : "+type.getTypeName());
        em.getTransaction().begin();
        T tFromDB = em.merge(t);
        em.getTransaction().commit();
        return tFromDB;
    }

    @Override
    public T update(T t) {
        log.info("Update entity : "+type.getTypeName());
        em.getTransaction().begin();
        T fromDB=em.merge(t);
        em.getTransaction().commit();
        return fromDB;
    }

    @Override
    public T getById(int id) {
        log.info("Get by id entity : "+type.getTypeName());
        return em.find(type, id);
    }

    @Override
    public List<T> getAll() {
        log.info("Get all  entity : "+type.getTypeName());
        TypedQuery<T> namedQuery = em.createNamedQuery(type.getTypeName()+".getAll", type);
        return namedQuery.getResultList();
    }
}
