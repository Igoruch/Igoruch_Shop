package dao;

import model.Category;
import org.apache.log4j.Logger;
import javax.persistence.TypedQuery;
import java.util.List;

public class DAOCategoryImpl extends GenericDaoImpl<Category> implements DAOCategory {

    private final String getAllCategoryQuery="select c from Category c";

    private static final Logger log = Logger.getLogger(DAOCategoryImpl.class);

    @Override
    public void deleteCategory(Category category) {
        log.info("Delete category! "+category.getName());
        em.getTransaction().begin();
        em.remove(category);
        em.getTransaction().commit();
    }

    @Override
    public List<Category> getPageOfCategory(int page,int count) {
        log.info("Get page category!"+"Page:"+page+" Count:"+count);
        TypedQuery<Category> namedQuery = em.createQuery(getAllCategoryQuery, Category.class)
                .setMaxResults(count)
                .setFirstResult(page * count);

        if (namedQuery.getResultList().isEmpty()){
            return null;
        }else return namedQuery.getResultList();
    }
}
