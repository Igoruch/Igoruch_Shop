package dao;

import model.Order;
import java.util.List;

public interface DAOOrder extends GenericDao<Order> {

    List<Order> getAllOrdersInProcess();

}
