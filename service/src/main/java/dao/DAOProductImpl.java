package dao;

import model.Product;
import org.apache.log4j.Logger;
import javax.persistence.*;
import java.util.List;

public class DAOProductImpl extends GenericDaoImpl<Product> implements DAOProduct {

    private final String getPageOfProductsByCategoryIdQuery = "select pr from Product pr where pr.category.id = :catId order by pr.id desc";
    private final String getProductsByCategoryIdQuery = "select pr from Product pr where pr.category.id = :catId order by pr.id desc";
    private final String getLastAddedProductsQuery = "select p from Product p where p.category!=null order by p.id desc";
    private final String getProductByOrderIdQuery = "SELECT p FROM Order o JOIN o.products p WHERE o.id = :idOrder";

    private static final Logger log = Logger.getLogger(DAOProductImpl.class);

    @Override
    public List<Product> getPageOfProductsByCategoryId(int categoryId, int page, int count) {
        log.info("Get page of products: page:"+page+" count:"+count);
        TypedQuery<Product> namedQuery = this.em.createQuery(getPageOfProductsByCategoryIdQuery, Product.class)
                .setParameter("catId",categoryId)
                .setMaxResults(count)
                .setFirstResult(page * count);

        if (namedQuery.getResultList().isEmpty()){
            return null;
        }else return namedQuery.getResultList();
    }

    @Override
    public List<Product> getProductsByCategoryId(int categoryId) {
        log.info("Get product by categoru id"+categoryId);
        TypedQuery<Product> namedQuery = em.createQuery(getProductsByCategoryIdQuery, Product.class)
                .setParameter("catId",categoryId);
        if (namedQuery.getResultList().isEmpty()){
            return null;
        }else return namedQuery.getResultList();
    }

    @Override
    public List<Product> getLastAddedProducts(int count) {
        log.info("Get last added products!");
        TypedQuery<Product> namedQuery = em
                .createQuery(getLastAddedProductsQuery, Product.class);
        return namedQuery.setMaxResults(count).getResultList();
    }

    @Override
    public List<Product> getProductByOrderId(int id) {
        log.info("Get products by order id:" +id);
        Query query = em.createQuery(getProductByOrderIdQuery);
        query.setParameter("idOrder", id);
        return (List<Product>) query.getResultList();
    }
}
