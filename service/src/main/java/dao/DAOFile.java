package dao;

import org.springframework.web.multipart.MultipartFile;


public interface DAOFile {

    void deleteFile(String path);

    void uploadImage(MultipartFile file,String path);
}
