package dao;

import model.User;


public interface DAOUser extends GenericDao<User> {

    User getUserByName(String name);

    User getUserByOrderId(int id);
}

