package controller;

import model.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import service.EntityServiceProduct;
import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
public class ProductConroller {

    @Autowired
    EntityServiceProduct entityServiceProduct;

    @RequestMapping(value="/product", method = RequestMethod.GET , produces = "application/json")
    public List<Product> getListProduct(){
        List<Product> products = entityServiceProduct.getAllProducts();
        return products;
    }

    @RequestMapping(value="/product/category/{categoryId}/{page}/{count}", method = RequestMethod.GET , produces = "application/json")
    public List<Product> getPageOfProductsByCategoryId(@PathVariable int categoryId,@PathVariable int page,@PathVariable int count){
        List<Product> users = entityServiceProduct.getPageOfProductsByCategoryId(categoryId,page,count);
        return users;
    }

    @RequestMapping(value="/product/{id}", method = RequestMethod.GET, produces = "application/json")
    public Product getProductById(@PathVariable int id){
        return entityServiceProduct.getProductById(id);
    }

    @RequestMapping(value="/maxProductsPage/category/{id}", method = RequestMethod.GET)
    public int getCountOfPageProductsByCategoryId(@PathVariable int id){
        return entityServiceProduct.getMaxProductsPageByCategoryId(id);
    }

    @RequestMapping(value = "product/{id}", method = RequestMethod.POST)
    public Product updateProduct(@RequestBody Product product,@PathVariable int id) {
        return entityServiceProduct.updateProduct(product,id);
    }

    @RequestMapping(value = "product/category/{id}", method = RequestMethod.POST, produces = "application/json")
    public Product createProduct(@PathVariable int id,@RequestBody Product product) {
        return entityServiceProduct.createProduct(id,product);
    }

    @RequestMapping(value = "product/{id}", method = RequestMethod.DELETE)
    public void deleteProductById(@PathVariable int id) {
        entityServiceProduct.deleteProduct(id);
    }

    @RequestMapping(value="product/uploadImage/product/{id}", method=RequestMethod.POST, produces = "text/plain" )
    public @ResponseBody String singleSave(@RequestParam("file") MultipartFile file,@PathVariable int id ){
        return entityServiceProduct.uploadImage(file,id);
    }

    @RequestMapping(value="/lastAddedProducts/{count}", method = RequestMethod.GET , produces = "application/json")
    public List<Product> getLastAddedProducts(@PathVariable int count){
        List<Product> products = entityServiceProduct.getLastAddedProducts(count);
        return products;
    }

    @RequestMapping(value = "order/product/order/{id}", method = RequestMethod.GET, produces = "application/json")
    public List<Product> getProductByOrderId(@PathVariable int id) {
        return entityServiceProduct.getProductByOrderId(id);
    }
}
