package controller;

import model.Category;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import service.EntityServiceCategory;
import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
public class CategoryController {

    @Autowired
    EntityServiceCategory entityServiceCategory;

    @RequestMapping(value="/category", method = RequestMethod.GET , produces = "application/json")
    public List<Category> getListOfCategory(){
        List<Category> categories = entityServiceCategory.getAllCategory();
        return categories;
    }

    @RequestMapping(value="/category/{id}", method = RequestMethod.GET, produces = "application/json")
    public Category getCategoryById(@PathVariable int id){
        return entityServiceCategory.getCategoryById(id);
    }

    @RequestMapping(value="/category/{page}/{count}", method = RequestMethod.GET, produces = "application/json")
    public List<Category> getPageOfCategory(@PathVariable int page,@PathVariable int count){
        return entityServiceCategory.getPageOfCategory(page,count);
    }

    @RequestMapping(value = "category/{id}", method = RequestMethod.POST)
    public Category updateCategory(@RequestBody Category category,@PathVariable int id) {
        return entityServiceCategory.updateCategory(category,id);
    }

    @RequestMapping(value = "category/create", method = RequestMethod.POST)
    public Category createCategory(@RequestBody Category category) {
        return entityServiceCategory.createCategory(category);
    }

    @RequestMapping(value="category/uploadImage/category/{id}", method=RequestMethod.POST, produces = "text/plain" )
    public @ResponseBody String singleSave(@RequestParam("file") MultipartFile file, @PathVariable int id ){
      return entityServiceCategory.uploadImage(file,id);
    }

    @RequestMapping(value = "category/{id}", method = RequestMethod.DELETE)
    public void deleteCategoryById(@PathVariable int id){
        entityServiceCategory.deleteCategory(id);
    }

    @RequestMapping(value="/maxCategoryPage", method = RequestMethod.GET)
    public int getMaxCategoryPage(){
        return entityServiceCategory.getMaxCategoryPage();
    }
}
