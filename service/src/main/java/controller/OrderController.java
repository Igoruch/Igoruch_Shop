package controller;

import model.Order;
import model.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.*;
import service.EntityServiceOrder;
import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
public class OrderController {

    @Autowired
    EntityServiceOrder entityServiceOrder;

    @RequestMapping(value="/order/inProcess", method = RequestMethod.GET , produces = "application/json")
    public List<Order> getListOfOrders(){
        List<Order> orders = entityServiceOrder.getAllOrdersInProcess();
        return orders;
    }

    @RequestMapping(value="/order/{id}", method = RequestMethod.GET, produces = "application/json")
    public Order getOrderById(@PathVariable int id){
        return entityServiceOrder.getOrderById(id);
    }

    @RequestMapping(value = "/order", method = RequestMethod.GET)
    public Order createOrder() {
        return entityServiceOrder.createOrder();
    }

    @RequestMapping(value = "/product/order/{id}", method = RequestMethod.POST)
    public Order addProductsInOrderById(@PathVariable int id,@RequestBody List<Product> products) {
        return entityServiceOrder.addProductsInOrder(id,products);
    }

    @RequestMapping(value = "/order/{id}", method = RequestMethod.POST)
    public Order updateOrderStatus(@RequestBody Order order,@PathVariable int id) {
        return entityServiceOrder.updateOrderStatus(order,id);
    }
}
