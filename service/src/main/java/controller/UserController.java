package controller;

import model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import service.EntityServiceUser;
import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
public class UserController {

    @Autowired
    EntityServiceUser entityServiceUser;

    @RequestMapping(value="user", method = RequestMethod.GET , produces = "application/json")
    public List<User> getListUser(){
        List<User> users = entityServiceUser.getAllUsers();
        return users;
    }

    @RequestMapping(value="user/{id}", method = RequestMethod.GET, produces = "application/json")
    public User getUserById(@PathVariable int id){
        return entityServiceUser.getUserById(id);
    }

    @RequestMapping(value = "user/{id}", method = RequestMethod.POST)
    public User updateUser(@RequestBody User user,@PathVariable int id) {
        return entityServiceUser.updateUser(user,id);
    }

    @RequestMapping(value = "user/byName", method = RequestMethod.POST)
    public User getUserByName(@RequestBody User user) {
        return entityServiceUser.getUserByName(user.getLogin());
    }

    @RequestMapping(value = "user", method = RequestMethod.POST)
    public User addUser(@RequestBody User user) {
        return entityServiceUser.addUser(user);
    }

    @RequestMapping(value="identifyUser", method = RequestMethod.GET, produces = "application/json")
    public User identifyUser() throws Exception{
        return entityServiceUser.identifyUser();
    }

    @RequestMapping(value = "user/order/{id}", method = RequestMethod.GET, produces = "application/json")
    public User getUserByOrderId(@PathVariable int id) {
        return entityServiceUser.getUserByOrderId(id);
    }

    @RequestMapping(value = "/registration", method = RequestMethod.POST, produces = "text/plain")
    public String  registrationUser(@RequestBody User user) {
        System.out.println(user.toString());
        return entityServiceUser.registrationUser(user);
    }

}
