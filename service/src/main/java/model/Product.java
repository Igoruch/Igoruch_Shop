package model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import javax.persistence.*;

@Entity
@Table(name = "pruducts")
@NamedQuery(name = "model.Product.getAll", query = "SELECT pr from Product pr")
@Data
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column(name = "description", length = 150)
    private String description;

    @Column(name = "name", length = 60)
    private String name;

    @Column(name = "urlToImg", length = 250)
    private String urlToImg;

    @Column(name = "imgName", length = 250)
    private String imgName;

    @Column(name = "price")
    private Double price ;

    @ManyToOne(fetch = FetchType.LAZY )
    @JoinColumn(name = "category_id")
    @JsonIgnore
    private Category category;

}
