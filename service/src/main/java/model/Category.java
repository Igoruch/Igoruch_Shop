package model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "category")
@NamedQuery(name = "model.Category.getAll", query = "SELECT c from Category c")
@Data
public class Category {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column(name = "name", length = 60)
    private String name;

    @Column(name = "urlToImg", length = 250)
    private String urlToImg;

    @Column(name = "imgName", length = 250)
    private String imgName;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "category",cascade = CascadeType.ALL)
    @JsonIgnore
    private Set<Product> productSet;

}
