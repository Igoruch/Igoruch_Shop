package model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.hibernate.annotations.Type;
import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Set;

@Entity
@Table(name = "user")
@NamedQuery(name = "model.User.getAll", query = "SELECT u from User u")
@Data
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column(name = "login", length = 32,unique = true)
    private String login;

    @Column(name = "role", length = 32)
    private String role="ROLE_USER";

    @Column(name = "firstName", length = 32)
    private String firstName;

    @Column(name = "lastName", length = 32)
    private String lastName;

    @Column(name = "address", length = 200)
    private String address;

    @Column(name = "password", length = 200)
    private String password;

    @Column(name = "email", length = 50)
    private String email;

    @Column(name = "phone", length = 32)
    private String phone;

    @Column(name = "indexAdress", length = 15)
    private String indexAdress;

    @Column(name = "enabled")
    private boolean enabled=true;

    @Column(name = "registrationDate" )
    @Type(type = "org.hibernate.type.LocalDateTimeType")
    private LocalDateTime registrationDate = LocalDateTime.now();

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "user",cascade = CascadeType.ALL)
    @JsonIgnore
    private Set<Order> ordersSet;
}
