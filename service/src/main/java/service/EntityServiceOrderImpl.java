package service;

import dao.DAOFactory;
import dao.DAOOrder;
import model.Order;
import model.Product;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;
import java.util.List;

public class EntityServiceOrderImpl implements EntityServiceOrder {

    @Autowired
    EntityServiceUser entityServiceUser;

    @Autowired
    private DAOFactory factoryDAO;

    DAOOrder orderDAO;

    @PostConstruct
    public void init(){
        orderDAO=factoryDAO.getOrderDAO();
    }

    @Override
    public Order createOrder() {
        Order order=new Order();
        order.setUser(entityServiceUser.identifyUser());
        return orderDAO.add(order);
    }

    @Override
    public Order addProductsInOrder(int orderId, List<Product> products) {
        Order order= getOrderById(orderId);
        order.setProducts(products);
        return orderDAO.update(order);
    }

    @Override
    public Order getOrderById(int id) {
        return orderDAO.getById(id);
    }

    @Override
    public List<Order> getAllOrdersInProcess() {
        return orderDAO.getAllOrdersInProcess();
    }

    @Override
    public Order updateOrderStatus(Order order,int id) {
        Order orderFromDb=orderDAO.getById(id);
        if (order!=null&&order.getStatus()!=null){
            orderFromDb.setStatus(order.getStatus());
        }
        return orderDAO.update(orderFromDb);
    }
}
