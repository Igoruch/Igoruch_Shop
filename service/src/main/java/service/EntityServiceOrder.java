package service;

import model.Order;
import model.Product;
import java.util.List;

public interface EntityServiceOrder {

    Order createOrder();

    Order addProductsInOrder(int orderId, List<Product> products);

    Order getOrderById(int id);

    List<Order> getAllOrdersInProcess();

    Order updateOrderStatus(Order order,int id);
}
