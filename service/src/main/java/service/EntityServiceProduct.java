package service;

import model.Product;
import org.springframework.web.multipart.MultipartFile;
import java.util.List;

public interface EntityServiceProduct {

    Product createProduct(int categoryId,Product product);

    void deleteProduct(int id);

    Product updateProduct(Product product,int id);

    Product getProductById(int id);

    List<Product> getAllProducts();

    List<Product> getPageOfProductsByCategoryId(int categoryId,int page,int count);

    int getMaxProductsPageByCategoryId(int id);

    List<Product> getLastAddedProducts(int count);

    List<Product> getProductByOrderId(int id);

    String uploadImage(MultipartFile file, int id);
}
