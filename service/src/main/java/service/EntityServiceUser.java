package service;

import model.User;

import java.util.List;

public interface EntityServiceUser {

    User addUser(User user);

    User updateUser(User user,int id);

    User getUserById(int id);

    User getUserByName(String name);

    User identifyUser();

    List<User> getAllUsers();

    String registrationUser(User user);

    User getUserByOrderId(int id);
}
