package service;

import dao.DAOFactory;
import dao.DAOFile;
import dao.DAOProduct;
import exception.EmptyFieldException;
import model.Product;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.PostConstruct;
import java.util.List;

public class EntityServiceProductImpl implements EntityServiceProduct {

    private final String path="D:/Igoruch_Shop/Igoruch_Shop/client/image/products/";

    private static final Logger log = Logger.getLogger(EntityServiceCategoryImpl.class);

    @Autowired
    private EntityServiceCategory entityServiceCategory;

    @Autowired
    private DAOFactory factoryDAO;

    private DAOProduct productDAO;

    private DAOFile fileDAO;

    @PostConstruct
    public void init(){
        productDAO=factoryDAO.getProductDAO();
        fileDAO=factoryDAO.getFileDAO();
    }



    @Override
    public Product createProduct(int categoryId,Product product) {
        if (product.getName()==null||product.getDescription()==null||product.getPrice()==null){
            throw new EmptyFieldException();
        }
        if (product.getPrice().isNaN()){
            throw new  NumberFormatException();
        }
        if (entityServiceCategory.getCategoryById(categoryId)==null){
            return null;
        }
        product.setCategory(entityServiceCategory.getCategoryById(categoryId));

        return productDAO.add(product);
    }

    @Override
    public void deleteProduct(int id) {
        Product product=getProductById(id);
        fileDAO.deleteFile(product.getUrlToImg() + product.getImgName());
        product.setCategory(null);
        productDAO.update(product);
    }

    @Override
    public Product updateProduct(Product product,int id) {

        Product productInDB=this.getProductById(id);
        if (product!=null) {
            if (product.getDescription()!=null) {
                productInDB.setDescription(product.getDescription());
            }
            if (product.getName()!=null) {
                productInDB.setName(product.getName());
            }
            if (product.getPrice()!=null) {
                productInDB.setPrice(product.getPrice());
            }
            if (product.getImgName()!=null) {
                fileDAO.deleteFile(productInDB.getUrlToImg()+productInDB.getImgName());
                productInDB.setImgName(product.getImgName());
            }
            if (product.getUrlToImg()!=null) {
                productInDB.setUrlToImg(product.getUrlToImg());
            }
        }
        return productDAO.update(productInDB);
    }

    @Override
    public Product getProductById(int id) {
        return productDAO.getById(id);
    }

    @Override
    public List<Product> getAllProducts() {
        return productDAO.getAll();
    }

    @Override
    public List<Product> getPageOfProductsByCategoryId(int categoryId, int page, int count) {
        return productDAO.getPageOfProductsByCategoryId(categoryId,page,count);
    }

    @Override
    public int getMaxProductsPageByCategoryId(int id) {
        try {
            int count = productDAO.getProductsByCategoryId(id).size();
            if (count != 0) {
                if (count % 8 == 0) {
                    return count / 8;
                } else return (count / 8) + 1;
            }
            return 0;
        }catch (NullPointerException e){
            log.error(e.getMessage());
            return 0;
        }
    }

    @Override
    public List<Product> getLastAddedProducts(int count) {
        return productDAO.getLastAddedProducts(count);
    }

    @Override
    public List<Product> getProductByOrderId(int id) {
        return productDAO.getProductByOrderId(id);
    }

    @Override
    public String uploadImage(MultipartFile file, int id) {
        String fileName;
        if (!file.isEmpty()) {
            fileDAO.uploadImage(file,path);
            fileName = file.getOriginalFilename();
            Product product=new Product();
            product.setImgName(fileName);
            product.setUrlToImg(path);
            updateProduct(product,id);
            return "You have successfully uploaded " + fileName;
        }else return "Error";
    }
}
