package service;

import dao.DAOCategory;
import dao.DAOFactory;
import dao.DAOFile;
import dao.DAOProduct;
import exception.EmptyFieldException;
import model.Category;
import model.Product;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.PostConstruct;
import java.util.List;


public class EntityServiceCategoryImpl implements EntityServiceCategory {

    private final String path = "D:/Igoruch_Shop/Igoruch_Shop/client/image/category/";

    private static final Logger log = Logger.getLogger(EntityServiceCategoryImpl.class);

    @Autowired
    private DAOFactory factoryDAO;

    private DAOCategory categoryDAO;

    private DAOProduct productDAO;

    private DAOFile fileDAO;

    @PostConstruct
    public void init(){
        productDAO=factoryDAO.getProductDAO();
        fileDAO=factoryDAO.getFileDAO();
        categoryDAO=factoryDAO.getCategoryDAO();
    }

    @Override
    public Category createCategory(Category category) {
        if (category == null||category.getName() == null){
            throw new EmptyFieldException();
        }
        return categoryDAO.add(category);
    }

    @Override
    public void deleteCategory(int id) {
        log.info("Delete category by id: "+id);
        if (productDAO.getProductsByCategoryId(id) != null) {
            for (Product product : productDAO.getProductsByCategoryId(id)) {
                fileDAO.deleteFile(product.getUrlToImg() + product.getImgName());
                product.setCategory(null);
                productDAO.update(product);
            }
        }
        Category category = categoryDAO.getById(id);
        if (category != null) {
            fileDAO.deleteFile(category.getUrlToImg() + category.getImgName());
            category.setProductSet(null);
            categoryDAO.deleteCategory(category);
        }
    }

    @Override
    public Category updateCategory(Category category, int id) {
        if (category.getName() == null && category.getImgName() == null && category.getUrlToImg() == null) {
            throw new EmptyFieldException();
        }
        Category categoryInDB = this.getCategoryById(id);
        if (category != null) {
            if (category.getName() != null) {
                categoryInDB.setName(category.getName());
            }
            if (category.getImgName() != null) {
                fileDAO.deleteFile(categoryInDB.getUrlToImg() + categoryInDB.getImgName());
                categoryInDB.setImgName(category.getImgName());
            }
            if (category.getUrlToImg() != null) {
                categoryInDB.setUrlToImg(category.getUrlToImg());
            }
        }
        return categoryDAO.update(categoryInDB);
    }

    @Override
    public Category getCategoryById(int id) {
        return categoryDAO.getById(id);
    }

    @Override
    public List<Category> getAllCategory() {
        return categoryDAO.getAll();
    }

    @Override
    public List<Category> getPageOfCategory(int page, int count) {
        return categoryDAO.getPageOfCategory(page, count);
    }

    @Override
    public int getMaxCategoryPage() {
        try {
            int count = categoryDAO.getAll().size();
            if (count % 6 == 0) {
                return count / 6;
            } else return (count / 6) + 1;
        }catch (NullPointerException e){
            log.error(e.getMessage());
            return 0;
        }
    }

    @Override
    public String uploadImage(MultipartFile file, int id) {
        String fileName ;
        if (!file.isEmpty()) {
            fileDAO.uploadImage(file,path);
            fileName = file.getOriginalFilename();
            Category category = new Category();
            category.setImgName(fileName);
            category.setUrlToImg(path);
            updateCategory(category, id);
            return "You have successfully uploaded " + fileName;
        }else return "Error";
    }
}
