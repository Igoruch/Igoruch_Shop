package service;

import model.Category;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface EntityServiceCategory {

    Category createCategory(Category category);

    void deleteCategory(int id);

    Category updateCategory(Category category,int id);

    Category getCategoryById(int id);

    List<Category> getAllCategory();

    List<Category> getPageOfCategory(int page,int count);

    int getMaxCategoryPage();

    String uploadImage(MultipartFile file,int id);
}
