package service;

import dao.DAOFactory;
import dao.DAOUser;
import model.User;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import javax.annotation.PostConstruct;
import java.util.List;

public class EntityServiceUserImpl implements EntityServiceUser {

    private static final Logger log = Logger.getLogger(EntityServiceUserImpl.class);

    @Autowired
    private DAOFactory factoryDAO;

    private DAOUser userDAO;

    @PostConstruct
    public void init(){
        userDAO=factoryDAO.getUserDAO();
    }

    @Override
    public User addUser(User user) {
        return userDAO.add(user);
    }

    @Override
    public User updateUser(User user,int id) {
        User userInDB=this.getUserById(id);
        if (user!=null) {
            if (user.getLogin()!=null) {
                userInDB.setLogin(user.getLogin());
            }
            if (user.getEmail()!=null) {
                userInDB.setEmail(user.getEmail());
            }
            if (user.getAddress()!=null) {
                userInDB.setAddress(user.getAddress());
            }
            if (user.getIndexAdress()!=null) {
                userInDB.setIndexAdress(user.getIndexAdress());
            }
            if (user.getPassword()!=null) {
                userInDB.setPassword(user.getPassword());
            }
            if (user.getPhone()!=null) {
                userInDB.setPhone(user.getPhone());
            }
            if (user.getFirstName()!=null){
                userInDB.setFirstName(user.getFirstName());
            }
            if (user.getLastName()!=null){
                userInDB.setLastName(user.getLastName());
            }
            userInDB.setEnabled(user.isEnabled());
        }
        return userDAO.update(userInDB);
    }

    @Override
    public User getUserById(int id) {
        return userDAO.getById(id);
    }
    @Override
    public User getUserByName(String name){
        return userDAO.getUserByName(name);
    }
    @Override
    public List<User> getAllUsers() {
        return userDAO.getAll();
    }

    @Override
    public User identifyUser() {
        try {
            org.springframework.security.core.userdetails.User user = (org.springframework.security.core.userdetails.User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            log.info("Identify user:" + user.toString());
            return userDAO.getUserByName(user.getUsername());
        }catch (ClassCastException e){
            log.error(e.getMessage());
            return null;
        }

    }

    @Override
    public String registrationUser(User user) {

        final String passwordRegex = "^(?=.*[0-9])(?=.*[a-z])(?=\\S+$).{8,}$";
        final String emailRegex = "^[a-zA-Z0-9_!#$%&'*+/=?`{|}~^.-]+@[a-zA-Z0-9.-]+$";

        if (user==null){
            return "Empty field";
        }
        if (user.getLogin()==null||user.getLogin().length()<6){
            return "Invalid login. Less then 6 characters";
        }
        if (getUserByName(user.getLogin())!=null){
            return "User with this login already exist!";
        }
        if (user.getPassword()==null){
            return "Invalid password!";
        }
        if (user.getEmail()==null||!user.getEmail().matches(emailRegex)){
            return "Invalid email!";
        }
        if (user.getFirstName()==null){
            return "Invalid first name!";
        }
        if (user.getLastName()==null){
            return "Invalid last name!";
        }
        if (user.getAddress()==null){
            return "Invalid address!";
        }
        if (user.getIndexAdress()==null||user.getIndexAdress().length()<5){
            return "Invalid index";
        }

        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        String hashedPassword = passwordEncoder.encode(user.getPassword());
        user.setPassword(hashedPassword);
        addUser(user);
        log.info("Registrane new user: "+user.toString());
        return  "Success registration!";
    }

    @Override
    public User getUserByOrderId(int id) {
        return userDAO.getUserByOrderId(id);
    }
}
