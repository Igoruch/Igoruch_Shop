
app.controller('updateCategoryController', ['$scope','$http','$routeParams','$route', function ($scope, $http,$routeParams,$route) {

    $scope.category = {};
    $scope.updateCat = null;


    $scope.updateCategory=function () {
        $scope.category.name=$scope.categoryName;

        $http({
            method: 'POST',
            url:'http://localhost:8080/category/'+$routeParams.id,
            withCredentials: true,
            data: $scope.category
        }).success(function (data) {
            $scope.updateCat=data;
            $scope.updateResult='Success!'
        }).error(function (data, status, header, config) {
            $scope.updateResult='Invalid data!'
            });
    };

    $scope.uploadUpdateCategoryFile = function() {
        $scope.updateCat.imgName = null;
        var fd = new FormData();
        //Take the first selected file
        fd.append("file", $scope.file[0]);
        $http.post('http://localhost:8080/category/uploadImage/category/'+$routeParams.id, fd, {
            withCredentials: true,
            headers: {'Content-Type': undefined },
            transformRequest: angular.identity
        }).then(function (data) {
            $scope.getCurrentCategory();
            $scope.updateCat.imgName = $scope.file[0].name;
            console.log($scope.updateCat.imgName);
            $scope.file = null;
        })

    };

    $scope.deleteCategory = function () {
        $http({
            method: 'DELETE',
            withCredentials: true,
            url: 'http://localhost:8080/category/' + $routeParams.id
        })
    };

    $scope.getCurrentCategory = function() {
        $http({
            method: 'GET',
            url:'http://localhost:8080/category/'+$routeParams.id,
            withCredentials: true
        }).success(function (data) {
            $scope.updateCat=data;
        });
    };

    $scope.getCurrentCategory();

    $scope.reloadPage = function(){$route.reload();};

    $scope.updateFile = function (file) {
        $scope.file=file;

    }
}]);