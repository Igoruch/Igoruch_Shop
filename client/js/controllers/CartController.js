
app.controller('CartController', ['$scope','$http','$route', function ($scope, $http,$route) {
    $scope.currentUser=JSON.parse(localStorage.getItem('currentUser'));
    $scope.cartArray=JSON.parse(localStorage.getItem('cart'));
    $scope.total=0;
    $scope.confirm=false;

    $scope.totolPageCount = function(price){
        $scope.total+=price;
    };

    $scope.remove = function(item) {
        var index = $scope.cartArray.indexOf(item);
        $scope.cartArray.splice(index, 1);
        localStorage.setItem('cart', JSON.stringify($scope.cartArray));
        $scope.reloadPage();
    };

    $scope.createOrder=function () {
        if ($scope.cartArray.length!=0) {
            $http({
                method: 'GET',
                url: 'http://localhost:8080/order',
                withCredentials: true
            }).success(function (data) {
                $scope.order = data;
                $scope.addProductInOrder();
            });
        }
    };

    $scope.addProductInOrder=function () {
            $http.post('http://localhost:8080/products/order/' + $scope.order.id, $scope.cartArray)
                .success(function (data, status, headers, config) {
                    var array = [];
                    localStorage.setItem('cart', JSON.stringify(array));
                })
    };

    $scope.orderInfo = function(){
        if ($scope.cartArray.length!=0) {
            $scope.confirm = true;
        }
    };

    $scope.reloadPage = function(){
        $route.reload();
    }

}]);