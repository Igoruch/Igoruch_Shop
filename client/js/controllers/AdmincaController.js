app.controller('AdmincaController', ['$scope','$http','$route', function ($scope, $http,$route) {

    $scope.user = {};
    $scope.userByLogin=null;
    $scope.activeButton='';
    $scope.allUsers=null;
    $scope.orderInProcess=null;
    $scope.total=0;

    $scope.GetAllUsers=function () {
        $scope.activeButton='AllUsers';
        $http({
            method: 'GET',
            url:'http://localhost:8080/user',
            withCredentials: true
        }).success(function (data) {
            $scope.userByLogin=null;
            $scope.allUsers=data;
        });
    };

    $scope.getUserByLogin=function (login) {
        $scope.user = {};
        $scope.allUsers=null;
        $scope.user.login=login;
        $http({
            method: 'POST',
            url:'http://localhost:8080/user/byName',
            withCredentials: true,
            data: $scope.user
        }).success(function (data) {
            $scope.userByLogin=data;
        })
    };

    $scope.banedUser=function (id) {
        $scope.user = {};
        $scope.user.enabled=false;
        $http({
            method: 'POST',
            url:'http://localhost:8080/user/' + id,
            withCredentials: true,
            data: $scope.user
        }).success(function (data) {
            if ($scope.userByLogin){
                $scope.getUserByLogin($scope.userByLogin.login)
            }else {
                $scope.GetAllUsers();
            }
        })
    };

    $scope.UnBanedUser=function (id) {
        $scope.user.enabled=true;
        $http({
            method: 'POST',
            url:'http://localhost:8080/user/' + id,
            withCredentials: true,
            data: $scope.user
        }).success(function (data) {
            if ($scope.userByLogin){
                $scope.getUserByLogin($scope.userByLogin.login)
            }else {
                $scope.GetAllUsers();
            }
        })
    };

    $scope.GetAllOrdersInProcess=function () {
        $scope.activeButton='Orders';
        $scope.productInOrder=null;
        $scope.userInOrder=null;
        $http({
            method: 'GET',
            url:'http://localhost:8080/order/inProcess',
            withCredentials: true
        }).success(function (data) {
            $scope.orderInProcess=data;
        });
    };

    $scope.GetUserByOrderId=function (id) {
        $scope.currentOrderId=id;
        $scope.orderInProcess=null;
        $http({
            method: 'GET',
            url:'http://localhost:8080/user/order/'+id,
            withCredentials: true
        }).success(function (data) {
            $scope.userInOrder=data;
            $scope.GetProductInOrderById(id)
        });
    };

    $scope.GetProductInOrderById=function (id) {
        $scope.total=0;
        $scope.orderInProcess=null;
        $http({
            method: 'GET',
            url:'http://localhost:8080/order/product/order/'+id,
            withCredentials: true
        }).success(function (data) {
            $scope.productInOrder=data;
        });
    };

    $scope.totolCount = function(price){
        $scope.total+=price;
    };

    $scope.orderDone=function () {
        $scope.orderDonee = {
            status:'DONE'
        };
        $http({
            method: 'POST',
            url:'http://localhost:8080/order/'+ $scope.currentOrderId,
            withCredentials: true,
            data: $scope.orderDonee
        }).success(function (data) {
            $scope.GetAllOrdersInProcess();
        })
    };
}]);
