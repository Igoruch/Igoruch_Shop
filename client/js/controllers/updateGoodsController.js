
app.controller('updateGoodsController', ['$scope','$http','$routeParams','$route', function ($scope, $http,$routeParams,$route) {
    $scope.product = {};
    $scope.newProduct = null;
    $scope.category=$routeParams.categoryId;
    $scope.pageNum=$routeParams.page;

    $scope.updateGoods=function () {
        $scope.product.name=$scope.goodsName;
        $scope.product.description=$scope.goodsDescription;
        $scope.product.price=$scope.goodsPrice;

        $http({
            method: 'POST',
            url:'http://localhost:8080/product/'+$routeParams.id,
            withCredentials: true,
            data: $scope.product
        }).success(function (data) {
            $scope.newProduct=data;
            $scope.result='Success update!';
            $scope.getCurrentProduct();
        })
            .error(function (data, status, header, config) {
                $scope.result='Invalid data!'
            });
    };

    $scope.uploadUpdateFile = function() {
        var fd = new FormData();
        //Take the first selected file
        fd.append("file", $scope.file[0]);
        $http.post('http://localhost:8080/product/uploadImage/product/'+$routeParams.id, fd, {
            withCredentials: true,
            headers: {'Content-Type': undefined },
            transformRequest: angular.identity
        }).success(function (data) {
            $scope.getCurrentProduct();
            $scope.currentProduct.imgName = $scope.file[0].name;
            console.log($scope.currentProduct.imgName);
            $scope.file = null;
        })
    };

    $scope.updateFileProduct = function (file) {
        $scope.file=file;

    };

    $scope.deleteProduct = function () {
        $http({
            method: 'DELETE',
            withCredentials: true,
            url: 'http://localhost:8080/product/' + $routeParams.id
        })
    };

    $scope.getCurrentProduct = function() {
        $http({
            method: 'GET',
            url:'http://localhost:8080/product/'+$routeParams.id,
            withCredentials: true
        }).success(function (data) {
            $scope.currentProduct=data;
        });
    };
    $scope.reloadPage = function(){
        $route.reload();
    };

    $scope.getCurrentProduct();
}]);