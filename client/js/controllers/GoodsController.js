
app.controller('GoodsController', ['$scope','$http','$routeParams', function ($scope, $http,$routeParams) {

    $scope.pageNumber=0;
    $scope.categoryId=$routeParams.categoryId;
    $scope.currentUser = JSON.parse(localStorage.getItem('currentUser'));

    $scope.GetProductById=function () {
        $http({
            method: 'GET',
            url:'http://localhost:8080/product/category/'+$routeParams.categoryId+'/'+$routeParams.page+'/'+$routeParams.count,
            withCredentials: true
        }).success(function (data) {
            $scope.goods=data;
        });
    };

    $scope.GetMaxPage=function () {
        $http({
            method: 'GET',
            url:'http://localhost:8080/maxProductsPage/category/'+$routeParams.categoryId,
            withCredentials: true
        }).success(function (data) {
            $scope.maxPage=data;
        });
    };

    $scope.GetProductById();
    $scope.GetMaxPage();

    $scope.incrementPage=function () {

        if(!(++$routeParams.page>=$scope.maxPage)) {
            $scope.pageNumber = $routeParams.page;
        }else $scope.decrementPage();
    };

    $scope.decrementPage=function () {
        if ($routeParams.page-1>=0) {
            $scope.pageNumber=--$routeParams.page;
        }
    };

}]);