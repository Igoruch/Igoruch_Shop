
app.controller('HomeController', ['$scope','$http','$window', function ($scope, $http,$window) {
    $scope.message = 'Hello from HomeController';
    $scope.barActive='home';
    $scope.currentUser=null;
    $scope.currentUserfromStorage=JSON.parse(localStorage.getItem('currentUser'));
    $scope.loginNow = false;

    $http({
        method: 'GET',
        url:'http://localhost:8080/identifyUser',
        withCredentials: true
    }).success(function (data) {
        if (!$scope.currentUserfromStorage&&$scope.loginNow === false&&data.id!=null) {
            $scope.loginNow = true;
        }
        localStorage.setItem('currentUser',JSON.stringify(data));
        $scope.currentUser=data;

            $scope.PreviousPathArray = JSON.parse(sessionStorage.getItem("PreviousPath"));
            if ($scope.PreviousPathArray && $scope.PreviousPathArray.length > 2 && $scope.loginNow === true) {
                $scope.redirectUrl = $scope.PreviousPathArray[$scope.PreviousPathArray.length - 2];
                window.location.href = $scope.redirectUrl;
                $scope.loginNow = null;
            }
    });

    $scope.GetLastAddedGoods=function () {
        $http({
            method: 'GET',
            url:'http://localhost:8080/lastAddedProducts/6',
            withCredentials: true
        }).success(function (data) {
            $scope.lastAddedGoods=data;
        });
    };

    $scope.GetLastAddedGoods();

    $scope.setCartArray=function () {
        var array = [];
        localStorage.setItem('cart', JSON.stringify(array));
    };

    if(!$scope.currentUser){
        localStorage.setItem('currentUser',null);
    }

    $scope.setCartArray();

    $scope.logout=function () {
        localStorage.setItem('currentUser',null);
        $http({
            method: 'GET',
            url:'http://localhost:8080/logout',
            withCredentials: true
        });
        $window.location.href = 'http://localhost:63343/client/index.html?_ijt=qju5v8e2doa2qg8u71somdm9br#/';
        $scope.reloadPage();
    };

    $scope.reloadPage = function(){$window.location.reload();};

    $scope.historyBack = function(){$window.history.back();}



}]);