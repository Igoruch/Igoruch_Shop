
app.controller('CategoriesController', ['$scope','$http','$route','$routeParams', function ($scope, $http,$route,$routeParams) {
    $scope.currentUser=JSON.parse(localStorage.getItem('currentUser'));
    $scope.pageNumber=0;

    $http.get('http://localhost:8080/category/'+$routeParams.pageNumber+'/'+$routeParams.count)
        .success(function (data) {
        $scope.categories = data;
    });

    $scope.GetMaxPage=function () {
        $http({
            method: 'GET',
            url:'http://localhost:8080/maxCategoryPage',
            withCredentials: true
        }).success(function (data) {
            $scope.maxPage=data;
        });
    };

    $scope.GetMaxPage();

    $scope.incrementPage=function () {
        if(!(++$routeParams.pageNumber>=$scope.maxPage)) {
            $scope.pageNumber=$routeParams.pageNumber;
        }else $scope.decrementPage();
    };

    $scope.decrementPage=function () {
        if ($routeParams.pageNumber-1>=0) {
            $scope.pageNumber=--$routeParams.pageNumber;
        }
    };

    $scope.reloadPage = function(){$route.reload();}
}]);
