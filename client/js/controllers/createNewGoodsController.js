
app.controller('createNewGoodsController', ['$scope','$http','$route','$routeParams', function ($scope, $http,$route,$routeParams) {

    $scope.product = {};
    $scope.newProduct = null;
    $scope.categoryId=$routeParams.categoryId;

     $scope.createNewGoods=function () {
     $scope.product.name=$scope.goodsName;
     $scope.product.description=$scope.goodsDescription;
     $scope.product.price=$scope.goodsPrice;
        $http({
            method: 'POST',
            url:'http://localhost:8080/product/category/'+$routeParams.categoryId,
            withCredentials: true,
            data: $scope.product
        }).success(function (data) {
            $scope.createData=data;
            $scope.newProduct=data;
            $scope.addProductResult='Success!';
        })
            .error(function (data, status, header, config) {
                $scope.addProductResult='Invalid data!';
            });
     };

    $scope.uploadFile = function() {
        var fd = new FormData();
        //Take the first selected file
        fd.append("file", $scope.file[0]);

        $http.post('http://localhost:8080/product/uploadImage/product/'+$scope.newProduct.id, fd, {
            withCredentials: true,
            headers: {'Content-Type': undefined },
            transformRequest: angular.identity
        }).success(function (data) {
            $scope.getCurrentProduct();
            $scope.newProduct.imgName = $scope.file[0].name;
            console.log($scope.newProduct.imgName);
            $scope.file = null;
        })
    };

    $scope.updateFileCreateProduct = function (file) {
        $scope.file=file;

    };

    $scope.getCurrentProduct = function() {
        $http({
            method: 'GET',
            url:'http://localhost:8080/product/'+$scope.newProduct.id,
            withCredentials: true
        }).success(function (data) {
            $scope.newProduct=data;
        });
    };

    $scope.reloadPage = function(){
        $route.reload();
    }
}]);