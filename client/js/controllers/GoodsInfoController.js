
app.controller('GoodsInfoController', ['$scope','$http','$routeParams','$window', function ($scope, $http,$routeParams,$window) {
    $scope.currentUser=JSON.parse(localStorage.getItem('currentUser'));
    $scope.category=$routeParams.categoryId;
    $scope.pageNum=$routeParams.page;
    $scope.GetProductById=function () {
        $http({
            method: 'GET',
            url:'http://localhost:8080/product/'+$routeParams.id,
            withCredentials: true
        }).success(function (data) {
            $scope.currentProduct=data;
        });
    };

    $scope.GetProductById();

    $scope.setCartArray=function (product) {
        var a = [];
        a = JSON.parse(localStorage.getItem('cart'));
        a.push(product);
        alert('Added in cart! Goods : '+product.name);
        localStorage.setItem('cart', JSON.stringify(a));
    };
}]);