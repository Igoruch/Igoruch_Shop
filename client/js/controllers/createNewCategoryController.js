
app.controller('createNewCategoryController', ['$scope','$http','$route', function ($scope, $http,$route) {

    $scope.category = {};
    $scope.newCategory = null;


    $scope.createNewCategory=function () {
        $scope.category.name=$scope.categoryName;

        $http({
            method: 'POST',
            url:'http://localhost:8080/category/create',
            withCredentials: true,
            data: $scope.category
        }).success(function (data) {
            $scope.newCategory=data;
        })
            .error(function (data, status, header, config) {
                $scope.addCategoryResult='Invalid data!';
            });
    };

    $scope.uploadCategoryFile = function() {
        var fd = new FormData();
        //Take the first selected file
        fd.append("file", $scope.file[0]);
        $http.post('http://localhost:8080/category/uploadImage/category/'+$scope.newCategory.id, fd, {
            withCredentials: true,
            headers: {'Content-Type': undefined },
            transformRequest: angular.identity
        }).success(function (data) {
            $scope.getCurrentCategory();
            $scope.newCategory.imgName = $scope.file[0].name;
            console.log($scope.newCategory.imgName);
            $scope.file = null;
        })
    };

    $scope.updateCategoryCreateFile = function (file) {
        $scope.file=file;
    };

    $scope.getCurrentCategory = function() {
        $http({
            method: 'GET',
            url:'http://localhost:8080/category/'+$scope.newCategory.id,
            withCredentials: true
        }).success(function (data) {
            $scope.newCategory=data;
        });
    };

    $scope.reloadPage = function(){$route.reload();}
}]);