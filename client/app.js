
var app = angular.module('myApp', ['ngRoute']);

app.config(function($routeProvider) {


    $routeProvider

        .when('/', {
            templateUrl : 'views/home.html',
            controller  : 'HomeController'
        })

        .when('/categories/:pageNumber/:count', {
            templateUrl : 'views/categories.html',
            controller  : 'CategoriesController'
        })

        .when('/goods/:categoryId/:page/:count', {
            templateUrl : 'views/goods.html',
            controller  : 'GoodsController'
        })
        .when('/goodsInfo/:id/:categoryId/:page', {
            templateUrl : 'views/goodsInfo.html',
            controller  : 'GoodsInfoController'
        })
        .when('/createNewGoods/:categoryId', {
            templateUrl : 'views/createNewGoods.html',
            controller  : 'createNewGoodsController'
        })
        .when('/createNewCategory', {
            templateUrl : 'views/createNewCategory.html',
            controller  : 'createNewCategoryController'
        })
        .when('/updateGoods/:id/:categoryId/:page', {
            templateUrl : 'views/updateGoods.html',
            controller  : 'updateGoodsController'
        })
        .when('/updateCategory/:id', {
            templateUrl : 'views/updateCategory.html',
            controller  : 'updateCategoryController'
        })
        .when('/cart', {
            templateUrl : 'views/cart.html',
            controller  : 'CartController'
        })
        .when('/registration', {
            templateUrl : 'views/registration.html',
            controller  : 'RegistrationController'
        })
        .when('/adminca', {
            templateUrl : 'views/adminca.html',
            controller  : 'AdmincaController'
        })
        .when('/contacts', {
            templateUrl : 'views/contacts.html'
        })
        .when('/login', {
            templateUrl : 'views/login.html'
        })

        .otherwise({redirectTo: '/'});

}).run(function ($rootScope,$location) {

    $rootScope.$on("$routeChangeSuccess",function(){

        var storedItem = sessionStorage.getItem("PreviousPath"),
            previousPath;
        if(storedItem){
            previousPath = JSON.parse(storedItem);
        }else{
            previousPath = [];
        }
        previousPath.push(window.location.href);
        sessionStorage.setItem('PreviousPath',JSON.stringify(previousPath));
    });

});




